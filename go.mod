module bitbucket.org/puzzle

go 1.14

require (
	fyne.io/fyne v1.3.1
	github.com/gotk3/gotk3 v0.4.0
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/oliamb/cutter v0.2.2
	gonum.org/v1/plot v0.7.0
)
