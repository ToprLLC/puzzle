package main

import (
	"flag"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/puzzle/game"
	"bitbucket.org/puzzle/visual"
	_ "bitbucket.org/puzzle/visual/fyne"
	_ "bitbucket.org/puzzle/visual/gtk"
)

func main() {
	flag.Parse()
	if flag.NArg() < 1 {
		fmt.Println("usage: puzzle [no of segments] [imagename] [fyne|gtk]")
		fmt.Println("       use . for segments=4 as well as etc/flower.png")
	}
	var puzSize = 4
	var image = "etc/flower.png"
	if flag.NArg() >= 1 {
		if strings.Compare(flag.Arg(0), ".") != 0 {
			ps, _ := strconv.ParseInt(flag.Arg(0), 10, 32)
			puzSize = int(ps)
		}
	}
	if flag.NArg() >= 2 {
		if strings.Compare(flag.Arg(1), ".") != 0 {
			image = flag.Arg(1)
		}
	}
	gametype := "fyne"
	if flag.NArg() >= 3 {
		if strings.Compare(flag.Arg(2), "gtk") == 0 {
			gametype = "gtk"
		}
	}

	//fmt.Printf("Image %s segments %d\n", image, puzSize)
	vis := visual.Lookup(gametype)
	_, g := game.NewImageGame(puzSize, image)
	vis.Play(g)
}
