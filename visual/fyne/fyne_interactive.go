package fyne

import (
	"fmt"
	"image/color"

	"bitbucket.org/puzzle/game"
	"fyne.io/fyne"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
)

type interactiveImage struct {
	widget.BaseWidget
	img    *canvas.Image
	min    fyne.Size
	Tapper func(fyne.Position)
}

type intImageRender struct {
	intimg    *interactiveImage
	imgrender *canvas.Image
}

func (r *interactiveImage) SetMinSize(size fyne.Size) {

	r.min = size
	r.Resize(r.min)
	fmt.Printf("SetMinSize %v\n", r.min)
}

func (r *interactiveImage) MinSize() fyne.Size {
	return fyne.NewSize(game.PUZZLESIZE, game.PUZZLESIZE)
}

func (r *interactiveImage) CreateRenderer() fyne.WidgetRenderer {
	return &intImageRender{intimg: r, imgrender: r.img}
}

func (r *interactiveImage) Tapped(ev *fyne.PointEvent) {
	fmt.Printf("tapped %v\n", ev)
	if r.Tapper == nil {
		fmt.Println("No handler")
	} else {
		r.Tapper(ev.Position)
	}
}

func (r *interactiveImage) TappedSecondary(ev *fyne.PointEvent) {
	fmt.Printf("tapped secondary %v\n", ev)
}

func newInteractiveImage(img *canvas.Image) *interactiveImage {
	r := &interactiveImage{img: img}
	r.ExtendBaseWidget(r)
	return r
}

func (r *interactiveImage) setImage(img *canvas.Image) {
	r.img = img
}
func (r *intImageRender) Refresh() {
	fmt.Println("Refresh")
	r.imgrender = r.intimg.img
	r.imgrender.Refresh()
}

func (r *intImageRender) Destroy() {
}

func (r *intImageRender) BackgroundColor() color.Color {
	return theme.BackgroundColor()
}

func (r *intImageRender) Layout(size fyne.Size) {
	r.imgrender.Resize(size)
}
func (r *intImageRender) MinSize() fyne.Size {
	return fyne.NewSize(game.PUZZLESIZE, game.PUZZLESIZE)
}

func (r *intImageRender) Objects() []fyne.CanvasObject {
	return []fyne.CanvasObject{r.imgrender}
}
