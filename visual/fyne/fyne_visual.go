package fyne

import (
	"fmt"
	"image"
	"time"

	"bitbucket.org/puzzle/game"
	"bitbucket.org/puzzle/visual"
	"fyne.io/fyne"
	"fyne.io/fyne/app"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/widget"
)

var myapp fyne.App

type FynePuzzle struct {
	MainWindow fyne.Window
	puzImage   *canvas.Image

	puzInteractive *interactiveImage
	puzzleBoard    *fyne.Container

	timerLabel *widget.Label
	movesLabel *widget.Label

	face *widget.Radio

	resetButton  *widget.Button
	jumbleButton *widget.Button

	imggam *game.ImageGame
}

var fynepuzzle FynePuzzle

func init() {
	fmt.Printf("Registering myself\n")
	visual.Register("fyne", &fynepuzzle)
}

func RefreshPuzzle() {

	fynepuzzle.imggam.Combine()
	fynepuzzle.puzImage = canvas.NewImageFromImage(fynepuzzle.imggam.Current)
	fynepuzzle.puzImage.Resize(fyne.Size{Width: game.PUZZLESIZE,
		Height: game.PUZZLESIZE})
	fynepuzzle.puzImage.Move(fyne.Position{0, 0})
	fynepuzzle.puzInteractive.setImage(fynepuzzle.puzImage)

	elapsed := time.Since(fynepuzzle.imggam.Game.Started)
	elapsedstr := fmt.Sprintf("Think time %2d:%2d s", int(elapsed.Minutes()), int(elapsed.Seconds()))
	if fynepuzzle.imggam.Game.Moves == 1 {
		fynepuzzle.imggam.Game.Started = time.Now()
	}
	//fynepuzzle.imggam.Game.Moves++
	fynepuzzle.movesLabel.SetText(fmt.Sprintf("Moves: %03d", fynepuzzle.imggam.Game.Moves))
	fynepuzzle.timerLabel.SetText(elapsedstr)

	fynepuzzle.MainWindow.Content().Refresh()
}
func imageClicked(pt fyne.Position) {
	err, addr := fynepuzzle.imggam.Locate(image.Point{X: pt.X, Y: pt.Y})
	fmt.Printf("%s - %v\n", err, addr)
	if err == nil {
		err = fynepuzzle.imggam.Game.Swap(addr)
		if err == nil {
			fmt.Printf("Swapped\n")
			RefreshPuzzle()
			if fynepuzzle.imggam.Game.Moves == 1 {
				fynepuzzle.imggam.Game.Started = time.Now()
			}
		} else {
			fmt.Printf("Ignored\n")
		}
	}

}

func resetButtonClicked() {
	fynepuzzle.imggam.Game.Reset()
	RefreshPuzzle()
}

func jumbleButtonClicked() {
	fynepuzzle.imggam.Game.Jumble(1024)
	RefreshPuzzle()
}

func faceChanged(newface string) {
	fmt.Printf("Face changed to %s\n", newface)
	switch newface {
	case visual.FaceChoices[0]:
		fynepuzzle.imggam.NumFace = false
	case visual.FaceChoices[1]:
		fynepuzzle.imggam.NumFace = true
	}
	RefreshPuzzle()
}

func (f *FynePuzzle) Play(imggam *game.ImageGame) {
	myapp = app.New()
	fynepuzzle.imggam = imggam
	fynepuzzle.MainWindow = myapp.NewWindow("Puzzle")

	fynepuzzle.face = widget.NewRadio(visual.FaceChoices, faceChanged)
	fynepuzzle.face.SetSelected(visual.FaceChoices[0])
	fynepuzzle.resetButton = widget.NewButton("Reset", resetButtonClicked)
	fynepuzzle.jumbleButton = widget.NewButton("Jumble", jumbleButtonClicked)

	toolbar := fyne.NewContainerWithLayout(layout.NewHBoxLayout(),
		fynepuzzle.face,
		fynepuzzle.resetButton,
		fynepuzzle.jumbleButton)

	fynepuzzle.movesLabel = widget.NewLabel("Moves :   0 ")
	fynepuzzle.timerLabel = widget.NewLabel("Time: ")
	statusbar := fyne.NewContainerWithLayout(layout.NewHBoxLayout(),
		fynepuzzle.movesLabel,
		fynepuzzle.timerLabel)

	fynepuzzle.puzImage = canvas.NewImageFromImage(imggam.Current)

	fynepuzzle.puzImage.Resize(fyne.Size{Width: game.PUZZLESIZE,
		Height: game.PUZZLESIZE})
	fynepuzzle.puzImage.Move(fyne.Position{0, 0})

	fynepuzzle.puzInteractive = newInteractiveImage(fynepuzzle.puzImage)
	fynepuzzle.puzInteractive.Resize(fynepuzzle.puzImage.Size())
	fynepuzzle.puzInteractive.Tapper = imageClicked

	fullwin := fyne.NewContainerWithLayout(layout.NewVBoxLayout(),
		toolbar,
		fynepuzzle.puzInteractive,
		statusbar)
	//imgwin)

	fynepuzzle.MainWindow.Resize(fyne.Size{Width: game.PUZZLESIZE,
		Height: game.PUZZLESIZE + visual.TOOLBARHEIGHT})

	fynepuzzle.MainWindow.SetContent(fullwin)
	fynepuzzle.MainWindow.ShowAndRun()

}
