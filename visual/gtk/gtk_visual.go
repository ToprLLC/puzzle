package gtk

import (
	"fmt"
	"image"
	"log"
	"time"

	"bitbucket.org/puzzle/game"
	"bitbucket.org/puzzle/visual"
	"github.com/gotk3/gotk3/cairo"
	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

type Puzzle struct {
	MainWindow *gtk.Window
	puzImage   *gtk.DrawingArea
	puzPixbuf  *gdk.Pixbuf

	timerLabel *gtk.Label
	movesLabel *gtk.Label

	face *gtk.ComboBoxText

	resetButton  *gtk.Button
	jumbleButton *gtk.Button

	imggam *game.ImageGame
}

var puzzle Puzzle

func init() {
	visual.Register("gtk", &puzzle)
}

func faceSelectionChanged() {
	newface := puzzle.face.GetActive()
	switch newface {
	case 0:
		puzzle.imggam.NumFace = false
	case 1:
		puzzle.imggam.NumFace = true
	}
	RefreshPuzzle()

}

func resetButtonClicked() {
	puzzle.imggam.Game.Reset()
	RefreshPuzzle()
}

func jumbleButtonClicked() {
	puzzle.imggam.Game.Jumble(1024)
	RefreshPuzzle()
}

func visualUpdates() bool {

	elapsed := time.Since(puzzle.imggam.Game.Started)
	elapsedstr := fmt.Sprintf("Think time %2d:%2d s", int(elapsed.Minutes()), int(elapsed.Seconds()))
	puzzle.timerLabel.SetText(elapsedstr)

	return true
}

func newPixbufFromImage(img image.Image) (pixbuf *gdk.Pixbuf) {

	pixbuf, _ = gdk.PixbufNew(gdk.COLORSPACE_RGB, true, 8, game.PUZZLESIZE, game.PUZZLESIZE)
	bounds := img.Bounds()
	rowstride := pixbuf.GetRowstride()
	nchannels := pixbuf.GetNChannels()
	pixels := pixbuf.GetPixels()

	for row := 0; row < bounds.Max.Y; row++ {
		for col := 0; col < bounds.Max.X; col++ {
			r, g, b, a := img.At(row, col).RGBA()
			pixels[col*rowstride+row*nchannels] = byte(r)
			pixels[col*rowstride+row*nchannels+1] = byte(g)
			pixels[col*rowstride+row*nchannels+2] = byte(b)
			pixels[col*rowstride+row*nchannels+3] = byte(a)
		}
	}
	return pixbuf
}

func repaintPuzzle(da *gtk.DrawingArea, cr *cairo.Context) {
	if puzzle.puzPixbuf != nil {
		gtk.GdkCairoSetSourcePixBuf(cr, puzzle.puzPixbuf, 0, 0)
		cr.Paint()
	}
}

func imageClicked(x, y int) {
	err, addr := puzzle.imggam.Locate(image.Point{X: x, Y: y})
	//fmt.Printf("%s - %v\n", err, addr)
	if err == nil {
		err = puzzle.imggam.Game.Swap(addr)
		if err == nil {
			//fmt.Printf("Swapped\n")
			RefreshPuzzle()
			if puzzle.imggam.Game.Moves == 1 {
				puzzle.imggam.Game.Started = time.Now()
			}
		} else {
			//fmt.Printf("Ignored\n")
		}
	}
}

func OnButtonPressed(da *gtk.DrawingArea, event *gdk.Event) {
	if event != nil {
		eventObject := &gdk.EventButton{event}
		if eventObject.Button() == 1 {
			x := int(eventObject.X())
			y := int(eventObject.Y())
			//fmt.Printf("Button Pressed %d,%d\n", x, y)
			imageClicked(x, y)
		}
	}
}

func RefreshPuzzle() {

	puzzle.imggam.Combine()
	puzzle.puzPixbuf = newPixbufFromImage(puzzle.imggam.Current)

	elapsed := time.Since(puzzle.imggam.Game.Started)
	elapsedstr := fmt.Sprintf("Think time %2d:%2d s", int(elapsed.Minutes()), int(elapsed.Seconds()))
	if puzzle.imggam.Game.Moves == 1 {
		puzzle.imggam.Game.Started = time.Now()
	}

	puzzle.movesLabel.SetText(fmt.Sprintf("Moves: %3d", puzzle.imggam.Game.Moves))
	puzzle.timerLabel.SetText(elapsedstr)

	puzzle.MainWindow.QueueDraw()
}
func (puz *Puzzle) Play(imggam *game.ImageGame) {
	puz.imggam = imggam
	var err error
	gtk.Init(nil)
	puzzle.MainWindow, err = gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
	if err != nil {
		log.Fatal("Unable to create window:", err)
	}
	puzzle.MainWindow.SetPosition(gtk.WIN_POS_CENTER)

	puzzle.MainWindow.SetTitle("Puzzle")

	puzzle.MainWindow.Connect("destroy", func() {
		gtk.MainQuit()
	})

	puzzle.face, _ = gtk.ComboBoxTextNew()
	for i, a := range visual.FaceChoices {
		puzzle.face.InsertText(i, a)
	}
	puzzle.face.SetActive(0)
	puzzle.face.Connect("changed", faceSelectionChanged)

	puzzle.resetButton, _ = gtk.ButtonNewWithLabel("Reset")
	puzzle.resetButton.Connect("clicked", resetButtonClicked)

	puzzle.jumbleButton, _ = gtk.ButtonNewWithLabel("Jumble")
	puzzle.jumbleButton.Connect("clicked", jumbleButtonClicked)

	menuBar, _ := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 5)
	menuBar.Add(puzzle.face)
	menuBar.Add(puzzle.resetButton)
	menuBar.Add(puzzle.jumbleButton)

	puzzle.puzImage, _ = gtk.DrawingAreaNew()
	puzzle.puzImage.SetSizeRequest(game.PUZZLESIZE, game.PUZZLESIZE)
	puzzle.puzImage.Connect("draw", repaintPuzzle)
	puzzle.puzImage.AddEvents(int(gdk.BUTTON_PRESS_MASK))
	puzzle.puzImage.Connect("button-press-event", OnButtonPressed, nil)

	//fmt.Printf("Creating pixbuf from %s\n", imggam.Filename)
	puzzle.puzPixbuf, _ = gdk.PixbufNewFromFile(imggam.Filename)

	puzzle.puzPixbuf = newPixbufFromImage(puzzle.imggam.Current)

	puzzle.timerLabel, _ = gtk.LabelNew("Think Time: ")
	puzzle.movesLabel, _ = gtk.LabelNew("Moves:    ")
	statusbar, _ := gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 5)
	statusbar.Add(puzzle.movesLabel)
	statusbar.Add(puzzle.timerLabel)

	fullwin, _ := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)

	fullwin.Add(menuBar)
	fullwin.Add(puzzle.puzImage)
	fullwin.Add(statusbar)

	puzzle.MainWindow.Add(fullwin)
	glib.TimeoutAdd(1000, visualUpdates)
	//fmt.Println("Initialized Gtk")

	puzzle.MainWindow.ShowAll()
	gtk.Main()
}
