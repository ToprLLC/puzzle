package visual

import (
	"fmt"

	"bitbucket.org/puzzle/game"
)

const (
	TOOLBARHEIGHT = 50
)

var FaceChoices = []string{"Graphic", "Numbers"}

//[]string

type Visualizer interface {
	Play(imggam *game.ImageGame)
}

var visualizers map[string](Visualizer)

func Register(name string, v Visualizer) {
	if visualizers == nil {
		visualizers = make(map[string]Visualizer)
	}
	visualizers[name] = v
	fmt.Printf("Registering %s\n", name)
}

func Lookup(name string) Visualizer {
	fmt.Printf("Looking up %s\n", name)
	return visualizers[name]
}
