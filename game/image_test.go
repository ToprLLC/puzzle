package game

import (
	"fmt"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"testing"
)

const pngimage = "../etc/flower.png"
const jpgimage = "../etc/puzzle.jpg"

func TestSetImage(t *testing.T) {
	g := NewGame(4)
	err, _ := g.SetImage(pngimage)
	if err != nil {
		t.Error(err)
	}
}

func TestNewImage(t *testing.T) {
	NewImageGame(5, pngimage)
}
func TestJumbleImage(t *testing.T) {
	g := NewGame(5)
	err, img := g.SetImage(pngimage)
	if err != nil {
		t.Error(err)
	}
	img.Game.Jumble(1024)
	img.Combine()
}

func TestLocate(t *testing.T) {
	g := NewGame(7)
	_, img := g.SetImage(jpgimage)
	var pts = []image.Point{{1, 1}, {100, 100}, {350, 350}, {450, 450}}
	for idx, pt := range pts {
		err, addr := img.Locate(pt)
		if err != nil {
			fmt.Printf("Point %d (%d,%d) is invalid %s\n", idx, pt.X, pt.Y, err)
		} else {
			fmt.Printf("Point %d (%d,%d) found %v\n", idx, pt.X, pt.Y, addr)
			_, state, face := g.Get(addr)
			fmt.Printf("State %d Face %d\n", state, face)
		}
	}
}
