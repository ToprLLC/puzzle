package game

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

var Debug bool
var DebugLevel = 1

type Addr struct {
	Row int
	Col int
}

type TileFace int

type SlotState int

const (
	INVISIBLE SlotState = iota
	VISIBLE
)

type Game struct {
	Moves   int
	Size    int
	Slots   []SlotState
	Tiles   []TileFace
	Started time.Time
}

var ErrInvalidAddress = errors.New("Invalid Address")
var ErrCannotSwap = errors.New("Cannot swap this tileface")
var ErrUnknown = errors.New("Unknown Error")

func init() {
	//Debug = true
}
func (g *Game) Idx(addr Addr) (error, int) {
	if addr.Row < 0 || addr.Col < 0 {
		return ErrInvalidAddress, -1
	}
	if g.Size <= addr.Row || g.Size <= addr.Col {
		return ErrInvalidAddress, -1
	}
	return nil, addr.Row*g.Size + addr.Col
}

func (g *Game) Set(addr Addr, state SlotState, tileface TileFace) error {
	err, idx := g.Idx(addr)
	if err != nil {
		return err
	}
	g.Slots[idx] = state
	g.Tiles[idx] = tileface
	return nil
}
func (g *Game) Get(addr Addr) (err error, state SlotState, tileface TileFace) {
	err, idx := g.Idx(addr)
	if err != nil {
		return err, INVISIBLE, -1
	}
	state = g.Slots[idx]
	tileface = g.Tiles[idx]

	return nil, state, tileface
}

func (g *Game) FindInvisible() Addr {
	for row := 0; row < g.Size; row++ {
		for col := 0; col < g.Size; col++ {
			_, st, _ := g.Get(Addr{Row: row, Col: col})
			if st == INVISIBLE {
				return Addr{Row: row, Col: col}
			}
		}
	}
	return Addr{Row: -1, Col: -1}
}

func (g *Game) Swap(addr Addr) error {
	err, state, tileface := g.Get(addr)
	if err != nil {
		return err
	}

	if state == INVISIBLE {
		return ErrCannotSwap
	}
	tgtslot := addr
	tgtslot.Row = addr.Row + 1
	err, s, t := g.Get(tgtslot)
	if err == nil {
		if s == INVISIBLE {
			g.Set(tgtslot, state, tileface)
			g.Set(addr, INVISIBLE, t)
			g.Moves++
			return nil
		}
	}
	tgtslot = addr
	tgtslot.Row = addr.Row - 1
	err, s, t = g.Get(tgtslot)
	if err == nil {
		if s == INVISIBLE {
			g.Set(tgtslot, state, tileface)
			g.Set(addr, INVISIBLE, t)
			g.Moves++
			return nil
		}
	}
	tgtslot = addr
	tgtslot.Col = addr.Col + 1
	err, s, t = g.Get(tgtslot)
	if err == nil {
		if s == INVISIBLE {
			g.Set(tgtslot, state, tileface)
			g.Set(addr, INVISIBLE, t)
			g.Moves++
			return nil
		}
	}

	tgtslot = addr
	tgtslot.Col = addr.Col - 1
	err, s, t = g.Get(tgtslot)
	if err == nil {
		if s == INVISIBLE {
			g.Set(tgtslot, state, tileface)
			g.Set(addr, INVISIBLE, t)
			g.Moves++
			return nil
		}
	}

	return ErrCannotSwap
}

func (g *Game) Reset() {
	for row := 0; row < g.Size; row++ {
		for col := 0; col < g.Size; col++ {
			g.Set(Addr{Row: row, Col: col}, VISIBLE, TileFace(row*g.Size+col+1))
		}
	}
	g.Set(Addr{Row: g.Size - 1, Col: g.Size - 1}, INVISIBLE, TileFace(g.Size*g.Size)-1)
	g.Started = time.Now()
	g.Moves = 0
}

func (g *Game) Jumble(times int) {
	fmt.Printf("Jumble %d\n", times)
	inv := g.FindInvisible()
	invnow := inv
	for t := times; t > 0; t-- {
		dir := rand.Int()
		switch dir % 4 {
		case 0:
			inv.Row = inv.Row - 1
		case 1:
			inv.Row = inv.Row + 1
		case 2:
			inv.Col = inv.Col - 1
		case 3:
			inv.Col = inv.Col + 1
		}
		err, _, _ := g.Get(inv)
		if err == nil {
			g.Swap(inv)
		} else {
			inv = invnow
		}
	}
	g.Moves = 0
	g.Started = time.Now()
}

func NewGame(size int) *Game {
	newgame := new(Game)
	newgame.Size = size
	newgame.Slots = make([]SlotState, size*size)
	newgame.Tiles = make([]TileFace, size*size)
	for row := 0; row < size; row++ {
		for col := 0; col < size; col++ {
			newgame.Set(Addr{Row: row, Col: col}, VISIBLE, TileFace(row*size+col+1))
		}
	}
	newgame.Set(Addr{Row: size - 1, Col: size - 1}, INVISIBLE, TileFace(size*size))
	newgame.Started = time.Now()
	return newgame
}

func (g *Game) Show() {
	for row := 0; row < g.Size; row++ {
		for col := 0; col < g.Size; col++ {
			//fmt.Printf("%d,%d : ", row, col)
			_, state, face := g.Get(Addr{Row: row, Col: col})
			if state == INVISIBLE {
				fmt.Printf("  .")
			} else {
				fmt.Printf(" %2d", face)
			}
		}
		fmt.Printf("\n")
	}
}
