package game

import (
	"fmt"
	"testing"
)

func TestNewGame(t *testing.T) {
	g := NewGame(5)
	g.Show()
}

func TestSwap(t *testing.T) {
	g := NewGame(4)
	err := g.Swap(Addr{1, 1})
	fmt.Printf("[1,1] : %s\n", err)
	err = g.Swap(Addr{2, 3})
	fmt.Printf("[2,3] %s\n", err)
	adr := g.FindInvisible()
	fmt.Printf("Invisible : %d,%d\n", adr.Row, adr.Col)
	g.Show()
	err = g.Swap(Addr{3, 0})
	fmt.Printf("[3,0] %s\n", err)
	adr = g.FindInvisible()
	fmt.Printf("Invisible : %d,%d\n", adr.Row, adr.Col)
	g.Show()
}

func TestJumble(t *testing.T) {
	g := NewGame(4)
	g.Jumble(512)
	g.Show()
}
