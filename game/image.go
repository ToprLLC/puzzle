package game

import (
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"log"
	"os"

	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/vgimg"

	"github.com/nfnt/resize"
	"github.com/oliamb/cutter"
)

const (
	PUZZLESIZE  = 500
	TILEBORDERS = 2
)

//const (
//	TILESIZE = 100
//)

type ImageGame struct {
	Game        *Game
	Filename    string
	Full        image.Image
	Segments    []image.Image
	NumSegments []image.Image
	Current     image.Image
	NumFace     bool
}

func snapshot(img image.Image, filename string) {
	if !Debug {
		return
	}
	puzimg, err := os.Create(filename)
	defer puzimg.Close()
	if err != nil {
		fmt.Printf("%s\n", filename)
		return
	}
	err = png.Encode(puzimg, img)
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}
	fmt.Printf("Image saved to %s\n", filename)
}

func NewImageGame(size int, filename string) (error, *ImageGame) {
	newgame := NewGame(size)
	err, imggame := newgame.SetImage(filename)
	imggame.AddNumberFaces()
	imggame.Filename = filename
	return err, imggame
}

func (g *ImageGame) AddNumberFaces() {
	tilesize := PUZZLESIZE / g.Game.Size
	font, _ := vg.MakeFont("Helvetica", 30.0)
	g.NumSegments = make([]image.Image, g.Game.Size*g.Game.Size)
	for row := 0; row < g.Game.Size; row++ {
		for col := 0; col < g.Game.Size; col++ {
			numpic := fmt.Sprintf("%2d", row*g.Game.Size+col+1)
			cnv := vgimg.New(vg.Length(tilesize), vg.Length(tilesize))
			cnv.FillString(font, vg.Point{X: vg.Length(tilesize) / 4.0,
				Y: vg.Length(tilesize)/4.0 + 30.0}, numpic)
			_, idx := g.Game.Idx(Addr{Col: col, Row: row})
			g.NumSegments[idx] = cnv.Image()
		}
	}
}
func (g *Game) SetImage(filename string) (error, *ImageGame) {
	newimg := new(ImageGame)
	newimg.Game = g
	imgfile, err := os.Open(filename)
	if err != nil {
		return err, nil
	}
	defer imgfile.Close()

	img, fmtName, err := image.Decode(imgfile)
	if err != nil {
		return err, nil
	}
	newimg.Full = img
	newimg.Filename = filename
	if Debug {
		fmt.Printf("Image %s format %s decoded\n", filename, fmtName)
		fmt.Printf("Bounds %v\n", newimg.Full.Bounds())
	}
	puzext := newimg.Full.Bounds()
	height := puzext.Max.Y - puzext.Min.Y
	width := puzext.Max.X - puzext.Min.X
	if Debug {
		fmt.Printf("Dimensions H=%d W=%d\n", height, width)
	}
	newrect := puzext
	if height > width {
		newrect.Min.Y = (puzext.Max.Y - puzext.Max.X) / 2
		newrect.Max.Y = puzext.Max.Y - newrect.Min.Y
		height = width
	} else {
		newrect.Min.X = (puzext.Max.X - puzext.Max.Y) / 2
		newrect.Max.X = newrect.Max.X - newrect.Min.X
		width = height
	}
	if Debug {
		fmt.Printf("%v\n", newrect)
	}
	cImg, err := cutter.Crop(img, cutter.Config{
		Height:  width,          // height in pixel or Y ratio(see Ratio Option below)
		Width:   height,         // width in pixel or X ratio
		Mode:    cutter.TopLeft, // Accepted Mode: TopLeft, Centered
		Anchor:  newrect.Min,    // Position of the top left point
		Options: 0,              // Accepted Option: Ratio
	})
	if err != nil {
		log.Fatal("Cannot crop image:", err)
	}
	snapshot(cImg, "puzimg.png")

	var puzimglen uint
	puzimglen = uint(PUZZLESIZE)

	imgResized := resize.Resize(puzimglen, puzimglen, cImg, resize.MitchellNetravali)
	puzimgresized, _ := os.Create("puzimgresized.png")
	defer puzimgresized.Close()
	err = png.Encode(puzimgresized, imgResized)

	newimg.Full = imgResized
	tilesize := PUZZLESIZE / g.Size
	newimg.Segments = make([]image.Image, g.Size*g.Size)
	for row := 0; row < g.Size; row++ {
		for col := 0; col < g.Size; col++ {
			segmentid := row*g.Size + col
			anchor := image.Point{X: col * tilesize, Y: row * tilesize}
			newimg.Segments[segmentid], err = cutter.Crop(newimg.Full,
				cutter.Config{
					Height:  tilesize,
					Width:   tilesize,
					Options: 0,
					Anchor:  anchor})
			if err != nil {
				fmt.Printf("%d,%d %s\n", row, col, err)
			} else {
				fn := fmt.Sprintf("tile_%d_%d.png", row, col)
				snapshot(newimg.Segments[segmentid], fn)
			}
		}
	}
	newimg.Combine()
	return nil, newimg

}

func (img *ImageGame) Combine() error {
	g := img.Game
	bigimg := image.NewNRGBA(image.Rectangle{Min: image.Point{0, 0},
		Max: image.Point{PUZZLESIZE, PUZZLESIZE}})
	if Debug {
		fmt.Printf("Big Image bounds %v\n", bigimg.Bounds())
	}
	tilesize := PUZZLESIZE / g.Size
	img.Current = bigimg
	for row := 0; row < g.Size; row++ {
		for col := 0; col < g.Size; col++ {
			tilerect := image.Rectangle{Min: image.Point{col*tilesize + 1, row*tilesize + 1},
				Max: image.Point{(col+1)*tilesize - 1, (row+1)*tilesize - 1}}
			if Debug {
				fmt.Printf("Target rectangle %v\n", tilerect)
			}
			_, idx := g.Idx(Addr{row, col})
			if g.Slots[idx] == VISIBLE {
				idxtile := g.Tiles[idx]
				if Debug {
					fmt.Printf("idx %d idxtile %d\n", idx, idxtile)
					fmt.Printf("Bounds of added segment %d %v\n", idx, img.Segments[idxtile].Bounds())
				}
				if !img.NumFace {
					draw.Draw(bigimg, tilerect, img.Segments[idxtile-1], img.Segments[idxtile-1].Bounds().Min, draw.Over)
				} else {
					draw.Draw(bigimg, tilerect, img.NumSegments[idxtile-1], img.NumSegments[idxtile-1].Bounds().Min, draw.Over)
				}
			} else {
				fmt.Printf("Not combining %d,%d\n", row, col)
			}
		}
	}
	g.Show()
	if Debug {
		snapshot(img.Current, "jumbled.png")
	}
	return nil
}

func (img *ImageGame) Locate(point image.Point) (err error, addr Addr) {
	tilesize := PUZZLESIZE / img.Game.Size
	x := point.X / tilesize
	y := point.Y / tilesize
	err, _ = img.Game.Idx(Addr{Row: y, Col: x})
	if err != nil {
		fmt.Printf("Locate returning %s\n", err)
		return err, Addr{Row: 0, Col: 0}
	}
	addr = Addr{Row: y, Col: x}
	fmt.Printf("Locate returning %s %v\n", err, addr)
	return nil, addr
}
